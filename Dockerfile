#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=node
ARG BASE_IMAGE_VERSION=8.17.0-alpine3.9

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
RUN apk --no-cache add \
        python2=2.7.18-r0 \
        make=4.2.1-r2 \
        g++=8.3.0-r0

RUN ln -sf /usr/bin/python2 /usr/bin/python

RUN npm install -g --unsafe-perm tradfri-mqtt@0.1.2

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

COPY --from=builder /usr/local/bin/node /usr/local/bin/node
COPY --from=builder /usr/local/lib/node_modules /usr/local/lib/node_modules

COPY image_files /

ENTRYPOINT [ "/entrypoint.sh" ]

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL   org.label-schema.name="homesmarthome/tradfri" \
        org.label-schema.description="Tradfri 2 MQTT Bridge" \
        org.label-schema.url="homesmarthome.5square.de" \
        org.label-schema.vcs-ref="$VCS_REF" \
        org.label-schema.vcs-url="$VCS_URL" \
        org.label-schema.vendor="fvsqr" \
        org.label-schema.version=$VERSION \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.schema-version="1.0" \
        org.label-schema.docker.cmd="TBD"