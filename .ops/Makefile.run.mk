docker_run:
	docker run -d --name=mosquitto_test_run -p 1883:1883 homesmarthome/mosquitto:latest
	docker run -d \
	  --name=tradfri_test_run \
	  -v $(PWD)/.env:/env \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep tradfri_test_run

docker_stop:
	docker rm -f tradfri_test_run 2> /dev/null ; true
	docker rm -f mosquitto_test_run 2> /dev/null; true