# Tradfri #

[![](https://images.microbadger.com/badges/image/homesmarthome/tradfri.svg)](https://microbadger.com/images/homesmarthome/tradfri "")

[![](https://images.microbadger.com/badges/version/homesmarthome/tradfri.svg)](https://microbadger.com/images/homesmarthome/tradfri "")

MQTT Bridge to IKEA Tradfri Gateway to control smart bulbs.

## Run Standalone
```
docker run -d \
  --name=tradfri \
  -v /config/env:/env \
  homesmarthome/tradfri
```
Following env vars are expected to be available from ```/env```file.
```
TRADFRI_GATEWAY="10.1.2.3"
TRADFRI_PSK="xyz"
```