#!/bin/sh

export $(< /env xargs)

node /usr/local/lib/node_modules/tradfri-mqtt/lib/cli.js \
    -g $TRADFRI_GATEWAY \
    -p $TRADFRI_PSK \
    -s /tmp \
    -a \
    tcp://${MQTT_HOST}:${MQTT_PORT}